# File for module specific CMake tests.

include(CheckSymbolExists)

option(DUNE_VENDOR_FMT_FORCE "Force vendoring of libfmt even if an installed version was found")

if (DUNE_VENDOR_FMT_FORCE)
  message(STATUS "Forced vendoring of {fmt}, will not look for installed version")
else()
  find_package(fmt CONFIG)
endif()

check_symbol_exists(localtime_r time.h DUNE_HAVE_LOCALTIME_R)

