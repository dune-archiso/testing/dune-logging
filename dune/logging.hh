// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_HH
#define DUNE_LOGGING_HH

#include <dune/logging/logging.hh>

namespace Dune::Logging {

  inline void init(const Logging::CollectiveCommunication& comm, const ParameterTree& params = {})
  {
    Logging::init(comm,params);
  }

  inline void shutdown()
  {
    Logging::shutdown();
  }

  inline bool initialized()
  {
    return Logging::initialized();
  }

  inline void registerSinkFactory(const std::string& name, Logging::SinkFactory sink_factory)
  {
    Logging::registerSinkFactory(name,sink_factory);
  }

  inline void shiftStartupTime(LogMessage::Duration offset)
  {
    Logging::shiftStartupTime(offset);
  }

  inline std::shared_ptr<Sink> makeSink(const std::string& name,const ParameterTree& params)
  {
    return Logging::makeSink(name,params);
  }

  inline void registerSink(std::shared_ptr<Sink> sink)
  {
    Logging::registerSink(sink);
  }

  inline std::shared_ptr<Sink> sink(const std::string& name)
  {
    return Logging::sink(name);
  }

  inline bool retireSink(std::string_view sink)
  {
    return Logging::retireSink(sink);
  }

  inline std::shared_ptr<ConsoleSink> cout()
  {
    return Logging::cout();
  }

  inline std::shared_ptr<ConsoleSink> cerr()
  {
    return Logging::cerr();
  }

  inline Logger logger()
  {
    return Logging::logger();
  }

  inline Logger logger(std::string_view backend)
  {
    return Logging::logger(backend);
  }

  inline Logger logger(const Dune::ParameterTree& params)
  {
    return Logging::logger(params);
  }

  inline Logger componentLogger(const Dune::ParameterTree& params, std::string_view preferred)
  {
    return Logging::componentLogger(params,preferred);
  }

  inline Logger registerBackend(std::string_view name, LogLevel level, bool attach_default_sinks = true)
  {
    return Logging::registerBackend(name,level,attach_default_sinks);
  }

  inline bool attachSink(std::string_view backend, std::string_view sink)
  {
    return Logging::attachSink(backend,sink);
  }

  inline bool detachSink(std::string_view backend, std::string_view sink)
  {
    return Logging::detachSink(backend,sink);
  }

  inline bool muted()
  {
    return Logging::muted();
  }

  inline void mute()
  {
    Logging::mute();
  }

  inline void unmute()
  {
    Logging::unmute();
  }

  inline void redirectCout(std::string_view backend, LogLevel level = LogLevel::notice, bool buffered = true)
  {
    Logging::redirectCout(backend,level,buffered);
  }

  inline void redirectCerr(std::string_view backend, LogLevel level = LogLevel::notice, bool buffered = true)
  {
    Logging::redirectCerr(backend,level,buffered);
  }

  inline void redirectClog(std::string_view backend, LogLevel level = LogLevel::notice, bool buffered = true)
  {
    Logging::redirectClog(backend,level,buffered);
  }

  inline void restoreCout()
  {
    Logging::restoreCout();
  }

  inline void restoreCerr()
  {
    Logging::restoreCerr();
  }

  inline void restoreClog()
  {
    Logging::restoreClog();
  }

  inline bool isCoutRedirected()
  {
    return Logging::isCoutRedirected();
  }

  inline bool isCerrRedirected()
  {
    return Logging::isCerrRedirected();
  }

  inline bool isClogRedirected()
  {
    return Logging::isClogRedirected();
  }

  template<typename Stream>
  inline void registerDebugStream(
    const std::string& name,
    Stream& stream,
    bool detach,
    LogLevel default_level,
    std::string_view default_backend = "default"
    )
  {
    Logging::registerDebugStream(name,stream,detach,default_level,default_backend);
  }

  inline const Logging::CollectiveCommunication& comm()
  {
    return Logging::comm();
  }

  inline LogMessage::Time startupTime(bool shifted = true)
  {
    return Logging::startupTime(shifted);
  }

  inline bool isValidName(std::string_view name)
  {
    return Logging::isValidName(name);
  }

  //! Logs a message to the default logger.
  template<typename... Args>
  void log(Args&&... args)
  {
    logger()(std::forward<Args>(args)...);
  }

} // Dune::Logging

#endif // DUNE_LOGGING_HH
