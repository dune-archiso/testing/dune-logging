// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include "config.h"

#include <dune/common/stdstreams.hh>

#include <dune/logging/debugstreamsupport.hh>
#include <dune/logging/exceptions.hh>
#include <dune/logging/logging.hh>

namespace Dune::Logging {


  // constructor for per-stream state
  DebugStreamState::StreamState::StreamState(
    LogLevel default_level,
    std::string_view default_backend,
    std::shared_ptr<LoggingStream> logging_stream,
    std::function<void(LoggingStream&)> attach,
    std::function<void()> detach
    )
    : default_level(default_level)
    , default_backend(default_backend)
    , logging_stream(std::move(logging_stream))
    , attach(std::move(attach))
    , detach(std::move(detach))
  {}


  DebugStreamState::StreamState::~StreamState()
  {
    // Detach if we are attached and have a detach function
    if (logging_stream and detach)
      detach();
  }

  void DebugStreamState::capture(const std::string& name, const ParameterTree& config)
  {
    try
    {
      auto log = Logging::logger("logging");
      auto& state = _streams.at(name);
      if (state.logging_stream)
        DUNE_THROW(LoggingError,"DebugStream already captured: " << name);
      Logger logger;
      bool line_buffered = true;
      bool enabled = true;
      if (config.hasSub(name))
      {
        auto& stream_config = config.sub(name);
        logger = Logging::logger(stream_config.get("backend",state.default_backend));
        if (stream_config.hasKey("level"))
          logger.setDefaultLevel(parseLogLevel(stream_config["level"]));
        else
          logger.setDefaultLevel(state.default_level);
        if (stream_config.hasKey("indent"))
          logger.setIndent(stream_config.get<int>("indent"));
        line_buffered = stream_config.get("line_buffered",true);
        enabled = stream_config.get("enabled",true);
      }
      else
      {
        bool is_enable_switch = false;
        try
        {
          enabled = config.get(name,true);
          is_enable_switch = true;
        }
        catch (Dune::RangeError&)
        {
          // do nothing, just take the default of true
        }
        LogLevel level = state.default_level;
        if (not is_enable_switch)
          level = parseLogLevel(config[name]);

        logger = Logging::logger(state.default_backend);
        logger.setDefaultLevel(level);
      }
      if (enabled)
      {
        state.logging_stream = std::make_shared<LoggingStream>(line_buffered,logger);
        state.attach(*state.logging_stream);
        log.info("Captured DebugStream {}"_fmt,name);
        log.debug("Configuration: backend={} level={} line_buffered={}"_fmt,logger.name(),logger.defaultLevel(),line_buffered);
      }
    }
    catch (std::out_of_range&)
    {
      DUNE_THROW(LoggingError,"Cannot capture DebugStream, not registered: " << name);
    }
  }

  DebugStreamState::DebugStreamState()
  {
    registerStream("dvverb",dvverb,true,LogLevel::debug,"default");
    registerStream("dverb",dverb,true,LogLevel::detail,"default");
    registerStream("dinfo",dinfo,true,LogLevel::notice,"default");
    registerStream("dwarn",dwarn,true,LogLevel::warning,"default");
    registerStream("derr",derr,true,LogLevel::error,"default");
    registerStream("dgrave",dgrave,true,LogLevel::critical,"default");
  }

  DebugStreamState::~DebugStreamState() = default;

  void DebugStreamState::captureStreams(const ParameterTree& config)
  {
    // capture all streams
    for (auto& [name, _] : _streams)
      capture(name,config);
  }

} // namespace Dune::Logging
