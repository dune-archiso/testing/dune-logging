#install headers
install(
  FILES
    checks.hh
    consolesink.hh
    debugstreamsupport.hh
    destructiblesingletonholder.hh
    exceptions.hh
    filesinks.hh
    fmt.hh
    logger.hh
    loggerbackend.hh
    logging.hh
    loggingstream.hh
    loggingstreambuffer.hh
    loglevel.hh
    logmessage.hh
    patternformatsink.hh
    sink.hh
    sinkmessageitems.hh
    type_traits.hh
    utility.hh
  DESTINATION
    ${CMAKE_INSTALL_INCLUDEDIR}/dune/logging
  )

dune_library_add_sources(
  dune-logging
  SOURCES
    debugstreamsupport.cc
    logger.cc
    logging.cc
    loggingstream.cc
    loggingstreambuffer.cc
    logmessage.cc
    patternformatsink.cc
    sink.cc
  )

# add_subdirectory(test)
