// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include "config.h"

#include <dune/logging/loggingstream.hh>

namespace Dune::Logging {

  LoggingStream::LoggingStream(bool line_buffered, Logger logger)
    : std::ostream(nullptr)
    , _stream_buf(line_buffered,logger)
  {
    // now we can set the streambuf pointer in the base class
    rdbuf(&_stream_buf);
  }

  void LoggingStream::setLogger(Logger logger)
  {
    _stream_buf.setLogger(logger);
  }

  Logger LoggingStream::logger() const
  {
    return _stream_buf.logger();
  }

} // namespace Dune::Logging
