// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_UTILITY_HH
#define DUNE_LOGGING_UTILITY_HH

#include <algorithm>
#include <cctype>
#include <memory>
#include <string_view>
#include <vector>

namespace Dune::Logging {

    /**
   * \addtogroup Utilities
   * \{
   */

  //! Trims a string_view of leading and trailing whitespace.
  std::string_view trim(std::string_view s)
  {
    auto isspace = [](unsigned char c) { return std::isspace(c); };
    auto front = std::find_if_not(begin(s),end(s),isspace);
    auto back = std::find_if_not(rbegin(s),rend(s),isspace).base();
    return front < back ? std::string_view(s.data() + (front - begin(s)),back-front) : std::string_view();
  }

  //! Parses a string with a list of delimited entries into a vector with the individual, trimmed entries.
  std::vector<std::string_view> parseConfigList(std::string_view list, char delim = ',')
  {
    std::vector<std::string_view> result;
    std::size_t front = 0;
    std::size_t back = 0;
    while ((back = list.find(delim,front)) != std::string::npos)
    {
      auto item = trim(list.substr(front,back-front));
      if (not item.empty())
        result.push_back(item);
      front = back + 1;
    }
    auto item = trim(list.substr(front));
    if (not item.empty())
      result.push_back(item);
    return result;
  }

  /**
   * \}
   */

} // namespace Dune::Logging

#endif // DUNE_LOGGING_UTILITY_HH
