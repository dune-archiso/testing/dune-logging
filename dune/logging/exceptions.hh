// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_EXCEPTIONS_HH
#define DUNE_LOGGING_EXCEPTIONS_HH

#include <dune/common/exceptions.hh>

namespace Dune::Logging {

  class LoggingError
    : public Exception
  {};

  class SingletonError
    : public Exception
  {};

} // namespace Dune::Logging

#endif // DUNE_LOGGING_EXCEPTIONS_HH
