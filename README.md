dune-logging
============

dune-logging is a fast, full-featured logging system for Dune. It consists of the following
components:

- `Logger` objects with value semantics for logging messages. Creates a cheap local check whether
  the message should be sent, actual processing is never inlined to speed up no-logging case.
- 'Sink' objects that process a log message, writing it to stdout, stderr, a file, a file per rank
  or even a network socket. Right now there are implementations for logging to a C stream and a file
  per rank. The logging system can be extended with other sinks without requiring recompilation.
- Backends, which are internal configuration objects that serve as targets for `Logger` objects and
  forward them to their configured set of `Sink` objects.
- A central singleton object for managing the state of the logging system. This is not
  user-accessible and only ever manipulated through static member functions or (for brevity)
  free-standing functions that forward to those member functions.

Features
--------

- There is minimal overhead in the non-logging case, only a cheap, inlined check.
- Log messages are formatted using the excellent [fmtlib](https://fmtlib.net) library, avoiding the
  very verbose state handling of the C++ streams. If it is found on the system, the system copy is
  used by default, otherwise dune-logging contains a vendored version of the library in
  `vendor/fmt` as a git submodule.
- It is possible to define a maximum log level at compile time, eliminating even that check.
- If the log message contains information that is expensive to calculate, the calculation can be
  delayed until after the check whether to log the message has been made by passing a lambda
  function to the logging call.
- The default sinks can be configured to log additional information using `fmtlib` syntax with a
  number of predefined formatting items.
- Additional sink types can easily be written and registered with the logging system. Afterwards,
  they can be configured through the `ParameterTree` used for setting up the logging system.
- The logging system will normally be configured through a `ParameterTree` which sets up backends
  and sinks and connects those to each other.
- Console sinks by default only log messages on rank 0, avoiding the noisy in-place checks for the
  MPI rank in the old setup.
- The sinks do not touch the C++ stream state, avoiding the need to save and restore that state.
- The logging system can optionally redirect the C++ standard streams through the logging system,
  making integration with code that still writes output to `std::cout` a lot easier.
- In a similar vein, the logging system will by default redirect all of the standard Dune debug
  streams (`Dune::dinfo` et. al.) through the logging system, mapping them to appropriate log
  levels. This can also be configured through the `ParameterTree`.
- The maximum detail level of log messages can be limited at compile time by defining the macro
  `DUNE_MAX_LOGLEVEL` to the name of the highest log level at which messages should be logged.
  This level can be set separately for each translation unit. Messages that are inhibited at compile
  time completely disappear from the binary, but computations for arguments of the log message still
  happen if the compiler cannot prove that there are no side effects.

Requirements
------------

dune-logging for now requires C++17.

Installation
------------

dune-logging is a standard Dune module, but make sure to use `git clone --recursive` when cloning
the repository to also clone the vendored `fmtlib` version. Otherwise installation will fail if CMake
cannot find a preinstalled version of `fmtlib`.

License
-------

dune-logging is distributed under the GNU LGPL 3, the GNU GPL 2 with a special exception, or the BSD
2-clause license, see [COPYING.md](COPYING.md) for detailed information. The vendored fmtlib is licensed
under the BSD 2-clause license, see [vendor/fmt/LICENSE.rst](vendor/fmt/LICENSE.rst) for details.
